# Author Marion Estoup
# Email marion_110@hotmail.fr
# June 2023

#########################################################  LINKS

# https://stackabuse.com/text-summarization-with-nltk-in-python/
# https://towardsdatascience.com/summarizing-medical-documents-with-nlp-85b14e4d9411
# https://github.com/sempwn/medical-text-nlp/blob/master/nbs/tutorial.ipynb
# https://www.analyticsvidhya.com/blog/2020/12/tired-of-reading-long-articles-text-summarization-will-make-your-task-easier/

# Other ways to translate text
# https://huggingface.co/docs/transformers/tasks/translation
# https://www.analyticsvidhya.com/blog/2021/06/language-translation-with-transformer-in-python/
# https://necromuralist.github.io/Neurotic-Networking/posts/nlp/english-to-french-data/
# https://blog.finxter.com/how-i-made-a-language-translator-using-python/
# https://www.andrewvillazon.com/clinical-natural-language-processing-python/
# https://www.iguazio.com/blog/top-22-free-healthcare-datasets-for-machine-learning/

# Other ways to summarize text
# https://towardsdatascience.com/document-summarization-using-latent-semantic-indexing-b747ef2d2af6
# https://iq.opengenus.org/latent-semantic-analysis-for-text-summarization/
# https://radimrehurek.com/gensim_3.8.3/summarization/summariser.html
# https://www.turing.com/kb/5-powerful-text-summarization-techniques-in-python


######################################################### LIBRARIES
# Web scraping
import bs4 as bs
import urllib.request
# Data cleaning and summarizing
import re
import nltk
import heapq
#!pip install lexrank
from lexrank import STOPWORDS, LexRank
# Translation of text
#!pip install -U deep-translator
from deep_translator import GoogleTranslator


######################################################## WEB SCRAPING



#################### BACTERIA TEXT
# Collect data from bacteria wikipedia page
bacteria = urllib.request.urlopen('https://en.wikipedia.org/wiki/Bacteria')
bacteria_data = bacteria.read()
bacteria_data = bs.BeautifulSoup(bacteria_data, 'lxml')
bacteria_data = bacteria_data.find_all('p')

# Create empty string that will be filled with the for loop right after
bacteria_text = ""

# For loop to collect text from bacteria_data
for p in bacteria_data:
    bacteria_text += p.text
    
# How many words in bacteria_text
len(bacteria_text.split()) 
# Result  7290




#################### VIRUS TEXT
# Collect data from virus wikipedia page
virus = urllib.request.urlopen('https://en.wikipedia.org/wiki/Virus#:~:text=Viruses%20are%20by%20far%20the,and%20many%20are%20species%2Dspecific.')
virus_data = virus.read()
virus_data = bs.BeautifulSoup(virus_data, 'lxml')
virus_data = virus_data.find_all('p')

# Create empty string that will be filled with the for loop right after
virus_text = ""

# For loop to collect text from virus_data
for p in virus_data:
    virus_text += p.text
    
# How many words in virus_text
len(virus_text.split()) 
# Result  8658
   

    
  


#################### ANTIBIOTIC TEXT 
# Collect data from Antibiotic wikipedia page
antibiotic = urllib.request.urlopen('https://en.wikipedia.org/wiki/Antibiotic')
antibiotic_data = antibiotic.read()
antibiotic_data = bs.BeautifulSoup(antibiotic_data, 'lxml')
antibiotic_data = antibiotic_data.find_all('p')

# Create empty string that will be filled with the for loop right after
antibiotic_text = ""

# For loop to collect text from antibiotic_data
for p in antibiotic_data:
    antibiotic_text += p.text
    
    
# How many words in antibiotic_text
len(antibiotic_text.split()) 
# Result  6514




#################### CANCER TEXT
# Collect data from Cancer wikipedia page
cancer = urllib.request.urlopen('https://en.wikipedia.org/wiki/Cancer')
cancer_data = cancer.read()
cancer_data = bs.BeautifulSoup(cancer_data, 'lxml')
cancer_data = cancer_data.find_all('p')

# Create empty string that will be filled with the for loop right after
cancer_text = ""

# For loop to collect text from cancer_data
for p in cancer_data:
    cancer_text += p.text
    
# How many words in cancer_text
len(cancer_text.split()) 
# Result  9695
    






#################### DNA TEXT
# Collect data from DNA wikipedia page
dna = urllib.request.urlopen('https://en.wikipedia.org/wiki/DNA')
dna_data = dna.read()
dna_data = bs.BeautifulSoup(dna_data, 'lxml')
dna_data = dna_data.find_all('p')

# Create empty string that will be filled with the for loop right after
dna_text = ""

# For loop to collect text from cancer_data
for p in dna_data:
    dna_text += p.text

# How many words in dna_text
len(dna_text.split()) 
# Result  8838






#################### PARKINSON'S DISEASE TEXT
# Collect data from Parkinson's disease wikipedia page
parkinson = urllib.request.urlopen('https://en.wikipedia.org/wiki/Parkinson%27s_disease')
parkinson_data = parkinson.read()
parkinson_data = bs.BeautifulSoup(parkinson_data, 'lxml')
parkinson_data = parkinson_data.find_all('p')

# Create empty string that will be filled with the for loop right after
parkinson_text = ""

# For loop to collect text from parkinson_data
for p in parkinson_data:
    parkinson_text += p.text
    
# How many words in parkinson_text
len(parkinson_text.split()) 
# Result 9375 
    



#################### HORMONE TEXT
# Collect data from Hormone wikipedia page
hormone = urllib.request.urlopen('https://en.wikipedia.org/wiki/Hormone')
hormone_data = hormone.read()
hormone_data = bs.BeautifulSoup(hormone_data, 'lxml')
hormone_data = hormone_data.find_all('p')

# Create empty string that will be filled with the for loop right after
hormone_text = ""

# For loop to collect text from hormone_data
for p in hormone_data:
    hormone_text += p.text
    
# How many words in hormone_text
len(hormone_text.split()) 
# Result  2511
    
 
    
 
  
#################### T CELL RECEPTOR TEXT
# Collect data from tcell_receptor wikipedia page
tcell_receptor = urllib.request.urlopen('https://en.wikipedia.org/wiki/T-cell_receptor')
tcell_receptor_data = tcell_receptor.read()
tcell_receptor_data = bs.BeautifulSoup(tcell_receptor_data, 'lxml')
tcell_receptor_data = tcell_receptor_data.find_all('p')

# Create empty string that will be filled with the for loop right after
tcell_receptor_text = ""

# For loop to collect text from tcell_receptor_data
for p in tcell_receptor_data:
    tcell_receptor_text += p.text
    
# How many words in tcell_receptor_text
len(tcell_receptor_text.split()) 
# Result 3609

  
  
#################### IMMUNE SYSTEM TEXT 
# Collect data from immune_system wikipedia page
immune_system = urllib.request.urlopen('https://en.wikipedia.org/wiki/Immune_system')
immune_system_data = immune_system.read()
immune_system_data = bs.BeautifulSoup(immune_system_data, 'lxml')
immune_system_data = immune_system_data.find_all('p')

# Create empty string that will be filled with the for loop right after
immune_system_text = ""

# For loop to collect text from immune_system
for p in immune_system_data:
    immune_system_text += p.text

# How many words in immune_system_text
len(immune_system_text.split()) 
# Result 7332



#################### ANTIHISTAMINE TEXT
# Collect data from Antihistamine wikipedia page
antihistamine = urllib.request.urlopen('https://en.wikipedia.org/wiki/Antihistamine')
antihistamine_data = antihistamine.read()
antihistamine_data = bs.BeautifulSoup(antihistamine_data, 'lxml')
antihistamine_data = antihistamine_data.find_all('p')

# Create empty string that will be filled with the for loop right after
antihistamine_text = ""

# For loop to collect text from Antihistamine
for p in antihistamine_data:
    antihistamine_text += p.text
    
    
# How many words in antihistamine_text
len(antihistamine_text.split()) 
# Result 1638


    
    
    
    


    
###################################################### CLEANING TEXTS


#################### BACTERIA TEXT
# Removing Square Brackets,and digits inside brackets, and Extra Spaces
bacteria_text = re.sub(r'\[[0-9]*\]', ' ', bacteria_text)
bacteria_text = re.sub(r'\s+', ' ', bacteria_text)
# This is the original article without the brackets

# Removing special characters and extra spaces
formated_bacteria_text = re.sub('[^a-zA-Z]', ' ', bacteria_text)
formated_bacteria_text = re.sub(r'\s+', ' ', formated_bacteria_text)
# This is the cleaned article without brackets, special characters, ...
    


#################### VIRUS TEXT
# Removing Square Brackets, and digits inside brackets and Extra Spaces
virus_text = re.sub(r'\[[0-9]*\]', ' ', virus_text)
virus_text = re.sub(r'\s+', ' ', virus_text)

# Removing special characters and extra spaces
formated_virus_text = re.sub('[^a-zA-Z]', ' ', virus_text)
formated_virus_text = re.sub(r'\s+', ' ', formated_virus_text)




#################### ANTIBIOTIC TEXT 
# Removing Square Brackets, and digits inside brackets, and Extra Spaces
antibiotic_text = re.sub(r'\[[0-9]*\]', ' ', antibiotic_text)
antibiotic_text = re.sub(r'\s+', ' ', antibiotic_text)

# Removing special characters and extra spaces
formated_antibiotic_text = re.sub('[^a-zA-Z]', ' ', antibiotic_text)
formated_antibiotic_text = re.sub(r'\s+', ' ', formated_antibiotic_text)




#################### CANCER TEXT
# Removing Square Brackets, and digits inside brackets, and Extra Spaces
cancer_text = re.sub(r'\[[0-9]*\]', ' ', cancer_text)
cancer_text = re.sub(r'\s+', ' ', cancer_text)

# Removing special characters and extra spaces
formated_cancer_text = re.sub('[^a-zA-Z]', ' ', cancer_text)
formated_cancer_text = re.sub(r'\s+', ' ', formated_cancer_text)




#################### DNA TEXT
# Removing Square Brackets, and digits inside brackets, and Extra Spaces
dna_text = re.sub(r'\[[0-9]*\]', ' ', dna_text)
dna_text = re.sub(r'\s+', ' ', dna_text)

# Removing special characters and extra spaces
formated_dna_text = re.sub('[^a-zA-Z]', ' ', dna_text)
formated_dna_text = re.sub(r'\s+', ' ', formated_dna_text)




#################### PARKINSON'S DISEASE TEXT
# Removing Square Brackets, and digits inside brackets, and Extra Spaces
parkinson_text = re.sub(r'\[[0-9]*\]', ' ', parkinson_text)
parkinson_text = re.sub(r'\s+', ' ', parkinson_text)

# Removing special characters and extra spaces
formated_parkinson_text = re.sub('[^a-zA-Z]', ' ', parkinson_text)
formated_parkinson_text = re.sub(r'\s+', ' ', formated_parkinson_text)




#################### HORMONE TEXT    
# Removing Square Brackets, and digits inside brackets, and Extra Spaces
hormone_text = re.sub(r'\[[0-9]*\]', ' ', hormone_text)
hormone_text = re.sub(r'\s+', ' ', hormone_text)

# Removing special characters and extra spaces
formated_hormone_text = re.sub('[^a-zA-Z]', ' ', hormone_text)
formated_hormone_text = re.sub(r'\s+', ' ', formated_hormone_text)




#################### T CELL RECEPTOR TEXT   
# Removing Square Brackets, and digits inside brackets, and Extra Spaces
tcell_receptor_text = re.sub(r'\[[0-9]*\]', ' ', tcell_receptor_text)
tcell_receptor_text = re.sub(r'\s+', ' ', tcell_receptor_text)

# Removing special characters and extra spaces
formated_tcell_text = re.sub('[^a-zA-Z]', ' ', tcell_receptor_text)
formated_tcell_text = re.sub(r'\s+', ' ', formated_hormone_text)




 
#################### IMMUNE SYSTEM TEXT 
# Removing Square Brackets, and digits inside brackets, and Extra Spaces
immune_system_text = re.sub(r'\[[0-9]*\]', ' ', immune_system_text)
immune_system_text = re.sub(r'\s+', ' ', immune_system_text)

# Removing special characters and extra spaces
formated_immune_system_text = re.sub('[^a-zA-Z]', ' ', immune_system_text)
formated_immune_system_text = re.sub(r'\s+', ' ', formated_immune_system_text)





################# ANTI HISTAMINE TEXT
# Removing Square Brackets, and digits inside brackets, and Extra Spaces
antihistamine_text = re.sub(r'\[[0-9]*\]', ' ', antihistamine_text)
antihistamine_text = re.sub(r'\s+', ' ', antihistamine_text)

# Removing special characters and extra spaces
formated_antihistamine_text = re.sub('[^a-zA-Z]', ' ', antihistamine_text)
formated_antihistamine_text = re.sub(r'\s+', ' ', formated_antihistamine_text)
    
    
    
    
    
    
######################################################## PROCESSINGS

#### Define stopwords 
stopwords = nltk.corpus.stopwords.words('english')

##### Create functions 
# Create function to find weighted frequency of occurrence for each word
def word_weight(text):
    for word in nltk.word_tokenize(text):
        if word not in stopwords:
            if word not in word_frequencies.keys():
                word_frequencies[word] = 1
            
            else:
                word_frequencies[word] += 1
                
                
                
# Create function to calculate sentence scores 
def sentence_weight(sentence):
    for sent in sentence:
        for word in nltk.word_tokenize(sent.lower()):
            if word in word_frequencies.keys():
                if len(sent.split(' ')) < 30: # To get words
                    if sent not in sentence_scores.keys():
                        sentence_scores[sent] = word_frequencies[word]
                    else:
                        sentence_scores[sent] += word_frequencies[word]




#################### BACTERIA TEXT
# Convert text to sentences
sentence_list_bacteria = nltk.sent_tokenize(bacteria_text)

# Apply word_weight function to calculate weight of words
word_frequencies = {}
word_weight(formated_bacteria_text)

# To find the weighted frequency, we can divide the number of occurences of all the words by the frequency of the most 
# occurring word
maximum_frequency = max(word_frequencies.values())
for word in word_frequencies.keys():
    word_frequencies[word] = (word_frequencies[word]/maximum_frequency)
    
# Apply sentence_weight function to calculate weight of sentences
sentence_scores = {}
sentence_weight(sentence_list_bacteria)

# Summarize text
summary_sentences = heapq.nlargest(7, sentence_scores, key=sentence_scores.get)

summary_bacteria = ' '.join(summary_sentences)
len(summary_bacteria.split()) # 152




#################### VIRUS TEXT
# Convert text to sentences
sentence_list_virus = nltk.sent_tokenize(virus_text)

# Apply word_weight function to calculate weight of words
word_frequencies = {}
word_weight(formated_virus_text)


# To find the weighted frequency, we can divide the number of occurences of all the words by the frequency of the most 
# occurring word
maximum_frequency = max(word_frequencies.values())
for word in word_frequencies.keys():
    word_frequencies[word] = (word_frequencies[word]/maximum_frequency)   
    
# Apply sentence_weight function to calculate weight of sentences
sentence_scores = {}
sentence_weight(sentence_list_virus)
    
# Summarize text
summary_sentences = heapq.nlargest(7, sentence_scores, key=sentence_scores.get)

summary_virus = ' '.join(summary_sentences)
len(summary_virus.split()) # 185

              
                    
           
                    
#################### ANTIBIOTIC TEXT 
# Convert text to sentences
sentence_list_antibiotic = nltk.sent_tokenize(antibiotic_text)

# Apply word_weight function to calculate weight of words
word_frequencies = {}
word_weight(formated_antibiotic_text)


# To find the weighted frequency, we can divide the number of occurences of all the words by the frequency of the most 
# occurring word
maximum_frequency = max(word_frequencies.values())
for word in word_frequencies.keys():
    word_frequencies[word] = (word_frequencies[word]/maximum_frequency)
    
    
# Apply sentence_weight function to calculate weight of sentences
sentence_scores = {}
sentence_weight(sentence_list_antibiotic)
  
# Summarize text
summary_sentences = heapq.nlargest(7, sentence_scores, key=sentence_scores.get)

summary_antibiotic = ' '.join(summary_sentences)
len(summary_antibiotic.split()) # 176





#################### CANCER TEXT
# Convert text to sentences
sentence_list_cancer = nltk.sent_tokenize(cancer_text)

# Apply word_weight function to calculate weight of words
word_frequencies = {}
word_weight(formated_cancer_text)


# To find the weighted frequency, we can divide the number of occurences of all the words by the frequency of the most 
# occurring word
maximum_frequency = max(word_frequencies.values())
for word in word_frequencies.keys():
    word_frequencies[word] = (word_frequencies[word]/maximum_frequency)
    
    
# Apply sentence_weight function to calculate weight of sentences
sentence_scores = {}
sentence_weight(sentence_list_cancer)
 
# Summarize text
summary_sentences = heapq.nlargest(7, sentence_scores, key=sentence_scores.get)

summary_cancer = ' '.join(summary_sentences)
len(summary_cancer.split()) # 140



#################### DNA TEXT
# Convert text to sentences
sentence_list_dna = nltk.sent_tokenize(dna_text)

# Apply word_weight function to calculate weight of words
word_frequencies = {}
word_weight(formated_dna_text)


# To find the weighted frequency, we can divide the number of occurences of all the words by the frequency of the most 
# occurring word
maximum_frequency = max(word_frequencies.values())
for word in word_frequencies.keys():
    word_frequencies[word] = (word_frequencies[word]/maximum_frequency)
    
    
# Apply sentence_weight function to calculate weight of sentences
sentence_scores = {}
sentence_weight(sentence_list_dna)
 
# Summarize text
summary_sentences = heapq.nlargest(7, sentence_scores, key=sentence_scores.get)

summary_dna = ' '.join(summary_sentences)
len(summary_dna.split()) # 180



#################### PARKINSON'S DISEASE TEXT
# Convert text to sentences
sentence_list_parkinson = nltk.sent_tokenize(parkinson_text)

# Apply word_weight function to calculate weight of words
word_frequencies = {}
word_weight(formated_parkinson_text)


# To find the weighted frequency, we can divide the number of occurences of all the words by the frequency of the most 
# occurring word
maximum_frequency = max(word_frequencies.values())
for word in word_frequencies.keys():
    word_frequencies[word] = (word_frequencies[word]/maximum_frequency)
    
    
# Apply sentence_weight function to calculate weight of sentences
sentence_scores = {}
sentence_weight(sentence_list_parkinson)
 
# Summarize text
summary_sentences = heapq.nlargest(7, sentence_scores, key=sentence_scores.get)

summary_parkinson = ' '.join(summary_sentences)
len(summary_parkinson.split()) # 156



#################### HORMONE TEXT 
# Convert text to sentences
sentence_list_hormone = nltk.sent_tokenize(hormone_text)

# Apply word_weight function to calculate weight of words
word_frequencies = {}
word_weight(formated_hormone_text)


# To find the weighted frequency, we can divide the number of occurences of all the words by the frequency of the most 
# occurring word
maximum_frequency = max(word_frequencies.values())
for word in word_frequencies.keys():
    word_frequencies[word] = (word_frequencies[word]/maximum_frequency)
    
    
# Apply sentence_weight function to calculate weight of sentences
sentence_scores = {}
sentence_weight(sentence_list_hormone)
 
# Summarize text
summary_sentences = heapq.nlargest(7, sentence_scores, key=sentence_scores.get)

summary_hormone = ' '.join(summary_sentences)
len(summary_hormone.split()) # 165




#################### T CELL RECEPTOR TEXT  
# Convert text to sentences
sentence_list_tcell = nltk.sent_tokenize(tcell_receptor_text)

# Apply word_weight function to calculate weight of words
word_frequencies = {}
word_weight(formated_tcell_text)


# To find the weighted frequency, we can divide the number of occurences of all the words by the frequency of the most 
# occurring word
maximum_frequency = max(word_frequencies.values())
for word in word_frequencies.keys():
    word_frequencies[word] = (word_frequencies[word]/maximum_frequency)
    
    
# Apply sentence_weight function to calculate weight of sentences
sentence_scores = {}
sentence_weight(sentence_list_tcell)
 
# Summarize text
summary_sentences = heapq.nlargest(7, sentence_scores, key=sentence_scores.get)

summary_tcell = ' '.join(summary_sentences)
len(summary_tcell.split()) # 168





#################### IMMUNE SYSTEM TEXT
# Convert text to sentences
sentence_list_immune_system = nltk.sent_tokenize(immune_system_text)

# Apply word_weight function to calculate weight of words
word_frequencies = {}
word_weight(formated_immune_system_text)


# To find the weighted frequency, we can divide the number of occurences of all the words by the frequency of the most 
# occurring word
maximum_frequency = max(word_frequencies.values())
for word in word_frequencies.keys():
    word_frequencies[word] = (word_frequencies[word]/maximum_frequency)
    
    
# Apply sentence_weight function to calculate weight of sentences
sentence_scores = {}
sentence_weight(sentence_list_immune_system)
 
# Summarize text
summary_sentences = heapq.nlargest(7, sentence_scores, key=sentence_scores.get)

summary_immune_system = ' '.join(summary_sentences)
len(summary_immune_system.split()) # 163




################# ANTI HISTAMINE TEXT
# Convert text to sentences
sentence_list_antihistamine = nltk.sent_tokenize(antihistamine_text)

# Apply word_weight function to calculate weight of words
word_frequencies = {}
word_weight(formated_antihistamine_text)


# To find the weighted frequency, we can divide the number of occurences of all the words by the frequency of the most 
# occurring word
maximum_frequency = max(word_frequencies.values())
for word in word_frequencies.keys():
    word_frequencies[word] = (word_frequencies[word]/maximum_frequency)
    
    
# Apply sentence_weight function to calculate weight of sentences
sentence_scores = {} # sentence_scores dictionary that will contain sentences with their corresponding score 
sentence_weight(sentence_list_antihistamine)
 
# Summarize text
summary_sentences = heapq.nlargest(7, sentence_scores, key=sentence_scores.get)

summary_antihistamine = ' '.join(summary_sentences)
len(summary_antihistamine.split()) # 136

                    
 


 
    
 


################################################################# ADDITIONAL PROCESSINGS (NOT REQUIRED)
 
################################################################# TRANSLATION OF SUMMARISED TEXTS

# https://towardsdatascience.com/how-to-detect-and-translate-languages-for-nlp-project-dfd52af0c3b5
# https://stackoverflow.com/questions/74241589/i-keep-getting-the-error-google-trans-new-google-trans-new-google-new-transerror
#from google_trans_new import google_translator
#translator = google_translator()
#summary_bacteria_french = translator.translate(summary_bacteria,lang_tgt='fr') 




#################### BACTERIA TEXT
summary_bacteria_french = GoogleTranslator(source='en', target='fr').translate(summary_bacteria)
summary_bacteria_spanish = GoogleTranslator(source='en', target='es').translate(summary_bacteria)
# How many words in summarised texts
len(summary_bacteria_french.split()) 
# 179
len(summary_bacteria_spanish.split()) 
# 181
# How many words in the original text : 7290
 

#################### VIRUS TEXT
summary_virus_french = GoogleTranslator(source='en', target='fr').translate(summary_virus)
summary_virus_spanish = GoogleTranslator(source='en', target='es').translate(summary_virus)
# How many words in summarised texts
len(summary_virus_french.split()) 
# 231
len(summary_virus_spanish.split()) 
# 237
# How many words in the original text : 8658



#################### ANTIBIOTIC TEXT 
summary_antibiotic_french = GoogleTranslator(source='en', target='fr').translate(summary_antibiotic)
summary_antibiotic_spanish = GoogleTranslator(source='en', target='es').translate(summary_antibiotic)
# How many words in summarised texts
len(summary_antibiotic_french.split()) 
# 219
len(summary_antibiotic_spanish.split()) 
# 212
# How many words in the original text : 6514



#################### CANCER TEXT
summary_cancer_french = GoogleTranslator(source='en', target='fr').translate(summary_cancer)
summary_cancer_spanish = GoogleTranslator(source='en', target='es').translate(summary_cancer)
# How many words in summarised texts
len(summary_cancer_french.split()) 
# 218
len(summary_cancer_spanish.split()) 
# 207
# How many words in the original text : 9695



#################### DNA TEXT
summary_dna_french = GoogleTranslator(source='en', target='fr').translate(summary_dna)
summary_dna_spanish = GoogleTranslator(source='en', target='es').translate(summary_dna)
# How many words in summarised texts
len(summary_dna_french.split()) 
# 181
len(summary_dna_spanish.split()) 
# 188
# How many words in the original text : 8838
    
    
    

#################### PARKINSON'S DISEASE TEXT
summary_parkinson_french = GoogleTranslator(source='en', target='fr').translate(summary_parkinson)
summary_parkinson_spanish = GoogleTranslator(source='en', target='es').translate(summary_parkinson)
# How many words in summarised texts
len(summary_parkinson_french.split()) 
# 201
len(summary_parkinson_spanish.split()) 
# 192
# How many words in the original text : 9375 



#################### HORMONE TEXT 
summary_hormone_french = GoogleTranslator(source='en', target='fr').translate(summary_hormone)
summary_hormone_spanish = GoogleTranslator(source='en', target='es').translate(summary_hormone)
# How many words in summarised texts
len(summary_hormone_french.split()) 
# 191
len(summary_hormone_spanish.split()) 
# 200
# How many words in the original text : 2511



#################### T CELL RECEPTOR TEXT  
summary_tcell_french = GoogleTranslator(source='en', target='fr').translate(summary_tcell)
summary_tcell_spanish = GoogleTranslator(source='en', target='es').translate(summary_tcell)
# How many words in summarised texts
len(summary_tcell_french.split()) 
# 195
len(summary_tcell_spanish.split()) 
# 205
# How many words in the original text : 3609



#################### IMMUNE SYSTEM TEXT
summary_immune_system_french = GoogleTranslator(source='en', target='fr').translate(summary_immune_system)
summary_immune_system_spanish = GoogleTranslator(source='en', target='es').translate(summary_immune_system)
# How many words in summarised texts
len(summary_immune_system_french.split()) 
# 202
len(summary_immune_system_spanish.split()) 
# 188
# How many words in the original text : 7332



################# ANTI HISTAMINE TEXT
summary_antihistamine_french = GoogleTranslator(source='en', target='fr').translate(summary_antihistamine)
summary_antihistamine_spanish = GoogleTranslator(source='en', target='es').translate(summary_antihistamine)
# How many words in summarised texts
len(summary_antihistamine_french.split()) 
# 175
len(summary_antihistamine_spanish.split()) 
# 165
# How many words in the original text : 1638


              





################################################################# ADDITIONAL PROCESSINGS (NOT REQUIRED)

################################################################# OTHER METHODS TO SUMMARIZE TEXTS

# Link : https://www.turing.com/kb/5-powerful-text-summarization-techniques-in-python
# https://pypi.org/project/lexrank/


#################### BACTERIA TEXT

# Remove default stopwords from our article
lxr = LexRank(bacteria_text, stopwords=STOPWORDS['en'])

# Get summary with classical LexRank algorithm
summary_bacteria_2 = lxr.get_summary(sentence_list_bacteria, summary_size=7,threshold=.1)
summary_bacteria_2 = ' '.join(summary_bacteria_2)

# Get summary with continuous LexRank
summary_bacteria_3 = lxr.get_summary(sentence_list_bacteria, threshold=None)
summary_bacteria_3 = ' '.join(summary_bacteria_3)

# We see that both summaries are different from our first summary. The first one seems better than this summary_bacteria_2 and
# than this summary_bacteria_3













           

            
