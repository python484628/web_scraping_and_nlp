# WebScraping_and_NLP

In this python file Projet_NLP.py, there is a code to collect 10 articles about different health topics (virus, bacteria, cancer, ...) thanks to web scraping. The second step consists of cleaning the articles in order to summarize them in the third step. The last part is about translating those articles' summaries in two other languages (french and spanish).


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

June 2023
